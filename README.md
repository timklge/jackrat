jackrat
============================

This library allows to construct backtracking top down packrat parsers in Kotlin using parser combination. Packrat parsing enables the parsing of PEG Grammars in linear time. Parsers are combinated using the following basic parsers:

- `AtomParser`: Matches only a specified UTF8 string
- `RegexParser`: Matches a regular expression
- `AndParser`: Matches a given list of parsers sequentially
- `OrParser`: Matches if any of a given list of parsers matches
- `KleeneParser`: Matches a parser 0 to `n` times, optionally separated by another parser
- `ManyParser`: Matches a parser 1 to `n` times, optionally separated by another parser
- `MaybeParser`: Matches a parser 0 or 1 times
- `EmptyParser`: Does not read any input and matches in every case
- `EndParser`: Matches only if the scanner has reached the end of the input string 

By default, `Atom` and `Regex` parsers skip (but do not match on) leading whitespace. This can be configured per parser.

If a parser matches, it returns a syntax tree `Node`. Every node points to the parser that produced it, the matched text, and a list of child nodes. Callbacks are not provided atm, so a full syntax tree traversal is needed to process the parse results.

To construct recursive parsers, create parser combinators with `nil` as the sub parser. After creating the sub parser that itself uses the parent parser, use the `Set` function on the parent parser to update its children. The [JSON parser](./json_test.go) provides an example for this.

This library is currently used in production, but some rarely used features may be broken. Additional documentation is ToDo.

Example
-----------

A full example in form of a working json parser is provided in [JsonTests.kt](https://git-st.inf.tu-dresden.de/timklge/jackrat/-/blob/master/src/commonTest/kotlin/JsonTests.kt).

```kotlin
val input = "Hello world"
val scanner = Scanner(input)
val helloParser = AtomParser("Hello", false)
val worldParser = AtomParser("world", false)
val helloAndWorldParser = AndParser(helloParser, worldParser)
val node = helloAndWorldParser.parse(scanner)

// node is the syntax tree root node
// node.children is a list containing a node for both the Hello and World parser
```

Use case
-----------
Using this library, you can dynamically define and parse PEG grammars at runtime. Parsing time is proportional to the input length and grammar complexity. Note that if you do not need to build grammars at runtime, consider using a standard LR parser generator.

Left recursion
-----------
Left recursion is supported via the method proposed by [Warth et al. (2008)](https://doi.org/10.1145/1328408.1328424).

License
------------
Licensed with MIT License. See [LICENSE](../../../LICENSE).