package jackrat

import de.timklge.jackrat.AndParser
import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.EndParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.fail

class EndTests {
    @Test
    fun EndTest(){
        val input = "Hello"
        val scanner = Scanner(input)
        val endParser = EndParser(false)
        val helloParser = AtomParser("Hello")
        val helloEndParser = AndParser(listOf(helloParser, endParser))
        val node = helloEndParser.parse(scanner)
        if(node.parser != helloEndParser) fail("End test combinator creates node with wrong parser")
        if(node.matched != input) fail("End test combinator doesn't match complete input")
        if(node.children.count() != 2) fail("End test combinator doesn't produce 3 children")
    }
}