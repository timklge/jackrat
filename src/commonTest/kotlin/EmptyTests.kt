package jackrat

import de.timklge.jackrat.AndParser
import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.EmptyParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.fail

class EmptyTests {
    @Test
    fun TestEmpty() {
        val input = "Hello"
        val scanner = Scanner(input)
        val emptyParser = EmptyParser()
        val helloParser = AtomParser("Hello")
        val emptyAndHelloParser = AndParser(listOf(emptyParser, helloParser))

        val node = emptyAndHelloParser.parse(scanner)
        if(node.parser != emptyAndHelloParser) fail("Empty Test combinator creates node with wrong parser")
        if(node.matched != "Hello") fail("Empty Test combinator doesn't match complete input")
        if(node.children.count() != 2) fail("Empty Test combinator child count doesn't match")
        if(node.children[0].parser != emptyParser || node.children[1].parser != helloParser) fail("Empty Test combinator children do not match")
    }

    @Test
    fun TestDoubleEmpty(){
        val input = ""
        val scanner = Scanner(input)
        val emptyParser = EmptyParser()
        val termParser = AndParser(listOf(emptyParser, emptyParser))
        val node = termParser.parse(scanner)
        if(node.parser != termParser) fail("Double empty parser creates node with wrong parser")
    }
}