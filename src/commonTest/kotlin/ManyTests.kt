package jackrat

import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.ManyParser
import de.timklge.jackrat.RegexParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class ManyTests {
    @Test
    fun ManyTest(){
        val input = "Hello Hello Hello"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val helloManyParser = ManyParser(helloParser)
        val node = helloManyParser.parse(scanner)
        assertEquals(helloManyParser, node.parser)
        assertEquals(3, node.children.count())
    }

    @Test
    fun ManySeparatorTest(){
        val input = "Hello, Hello, Hello"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val sepParser = AtomParser(",", false)
        val helloManyParser = ManyParser(helloParser, sepParser)
        val node = helloManyParser.parse(scanner)
        assertEquals(helloManyParser, node.parser, "Many combinator creates node with wrong parser")
        assertEquals("Hello,Hello,Hello", node.matched, "Many combinator doesn't match complete input")
        assertEquals(5, node.children.count(), "Many combinator doesn't produce five children")
    }

    @Test
    fun ManySeparatorRegexTest(){
        val input = "         23,  45"
        val scanner = Scanner(input)
        val digitParser = RegexParser("""\d+""")
        val digitSepParser = ManyParser(digitParser, AtomParser(","))
        val node = digitSepParser.parse(scanner)
        assertEquals(digitSepParser, node.parser, "Many separator combinator creates node with wrong parser")
        assertEquals("23,45", node.matched)
        assertEquals(3, node.children.count())
    }

    @Test
    fun ManyNoKleeneTest(){
        val input = "test"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello")
        val node = helloParser.parsePartial(scanner)
        assertNull(node)
    }
}