package jackrat

import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.OrParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals

class OrTests {
    @Test
    fun TestOr() {
        val input = "World"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", true)
        val worldParser = AtomParser("World", true)
        val helloOrWorldParser = OrParser(listOf(helloParser, worldParser))
        val node = helloOrWorldParser.parse(scanner)
        assertEquals(node.parser, helloOrWorldParser)
        assertEquals(1, node.children.count())
    }
}