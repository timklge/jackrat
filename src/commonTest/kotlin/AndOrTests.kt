package jackrat

import de.timklge.jackrat.AndParser
import de.timklge.jackrat.AtomParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.fail

class AndOrTests {
    @Test
    fun TestAndInsensitive() {
        val input = "HELLO world"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", true)
        val worldParser = AtomParser("World", true)
        val helloAndWorldParser = AndParser(listOf(helloParser, worldParser))
        val node = helloAndWorldParser.parse(scanner)
        if(node.parser != helloAndWorldParser) fail("And combinator creates node with wrong parser")
        if(node.matched != "HELLOworld") fail("And combinator doesn't match complete input")
    }
}