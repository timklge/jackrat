package jackrat

import de.timklge.jackrat.AndParser
import de.timklge.jackrat.AtomParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals

class AndTests {
    @Test
    fun TestAndInsensitive(){
        val input = "HELLO world"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", true)
        val worldParser = AtomParser("World", true)
        val helloAndWorldParser = AndParser(listOf(helloParser, worldParser))
        val node = helloAndWorldParser.parse(scanner)
        assertEquals(node.parser, helloAndWorldParser)
        assertEquals(2, node.children.count())
    }

    @Test
    fun TestAnd(){
        val input = "Hello world"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val worldParser = AtomParser("world", false)
        val helloAndWorldParser = AndParser(listOf(helloParser, worldParser))
        val node = helloAndWorldParser.parse(scanner)
        assertEquals(node.parser, helloAndWorldParser)
        assertEquals(2, node.children.count())
    }
}