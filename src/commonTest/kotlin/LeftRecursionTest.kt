package jackrat

import de.timklge.jackrat.*
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals

class LeftRecursionTest {
    @Test
    fun TestLeftRecursion(){
        val input = "5-1-4-3"
        val scanner = Scanner(input)
        val emptyParser = EmptyParser()
        val emptyParser1 = AndParser(listOf(emptyParser))

        val numParser = RegexParser("""\d+""")
        val numCombo1 = AndParser(listOf(emptyParser1, emptyParser1, numParser))
        val minusParser = AtomParser("-")

        val termParser = AndParser(listOf())
        val exprParser = OrParser(listOf(termParser, numCombo1))
        termParser.children = listOf(exprParser, minusParser, numCombo1)
        val node = exprParser.parse(scanner)
        assertEquals(node.parser, exprParser)
    }
}