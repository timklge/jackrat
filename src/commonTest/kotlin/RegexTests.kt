package jackrat

import de.timklge.jackrat.RegexParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertNull

class RegexTests {
    @Test
    fun TestRegex(){
        val input = "-3.4"
        val scanner = Scanner(input)
        val numParser = RegexParser("""-?\d+\.\d+""")
        numParser.parse(scanner)
    }

    @Test
    fun TestIrregularRegex(){
        val input = "3,4"
        val scanner = Scanner(input)
        val numParser = RegexParser("""-?\d+\.\d+""")
        val node = numParser.parsePartial(scanner)
        assertNull(node)
    }
}