package jackrat

import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.KleeneParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.fail

class KleeneTests {
    @Test
    fun KleeneTest(){
        val input = "Hello Hello Hello"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val helloKleeneParser = KleeneParser(helloParser)
        val node = helloKleeneParser.parse(scanner)
        assertEquals(helloKleeneParser, node.parser)
        assertEquals(3, node.children.count())
    }

    @Test
    fun KleeneIrregularTest(){
        val helloParser = AtomParser("Hello")
        val irregularInput = "Sonne"
        val irregularScanner = Scanner(irregularInput)
        val irregularParser = KleeneParser(helloParser)
        val irregularNode = irregularParser.parsePartial(irregularScanner)
        assertNotNull(irregularNode)
        assertEquals(irregularParser, irregularNode.parser, "Irregular kleene parser creates node with wrong parser")
        assertEquals(0, irregularNode.children.count(), "Irregular kleene parser doesn't produce zero children for irregular input")
    }

    @Test
    fun KleeneSeparatorTest(){
        val input = "Hello, Hello, Hello"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val sepParser = AtomParser(",", false)
        val helloKleeneParser = KleeneParser(helloParser, sepParser)
        val node = helloKleeneParser.parse(scanner)
        assertEquals(helloKleeneParser, node.parser, "Kleene combinator creates node with wrong parser")
        assertEquals("Hello,Hello,Hello", node.matched, "Kleene combinator doesn't match complete input")
        assertEquals(5, node.children.count(), "Kleene combinator doesn't produce five children")
    }
}