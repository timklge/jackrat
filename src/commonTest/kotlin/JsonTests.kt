package jackrat

import de.timklge.jackrat.*
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test

class JsonTests {
    @Test
    fun TestJSON(){
        val input = """
        {"menu": {
		"header": "Test",
		"items": [
			{"id": "1"},
			{"id": "2", "label": "Open New"},
			null,
			{"id": "a", "label": "In"},
			{"id": "b", "label": "Out"},
			{"id": "3", "label": "Original 4"},
			null,
			{"id": "5"},
			{"id": "6"},
			{"id": "7", "value": 3.451},
			null,
			null,
			{"id": "8", "boolean": false},
			{"id": "9", "label": "About..."}
		]
	}}
    """.trimIndent()
        val scanner = Scanner(input)
        val stringParser = AndParser(listOf(AtomParser("\""), RegexParser("""(?:[^"\\]|\\.)*"""), AtomParser("\"")))
        val valueParser = OrParser(listOf())
        val propParser = AndParser(listOf(stringParser, AtomParser(":"), valueParser))
        val objParser = AndParser(listOf(AtomParser("{"), KleeneParser(propParser, AtomParser(",")), AtomParser("}")))
        val nullParser = AtomParser("null")
        val numParser = RegexParser("""-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+-]?\d+)?""")
        val boolParser = RegexParser("(true|false)")
        val arrayParser = AndParser(listOf(AtomParser("["), KleeneParser(valueParser, AtomParser(",")), AtomParser("]")))
        valueParser.children = listOf(nullParser, objParser, stringParser, numParser, boolParser, arrayParser)
        valueParser.parse(scanner)
    }
}