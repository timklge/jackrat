package jackrat

import de.timklge.jackrat.AtomParser
import de.timklge.jackrat.MaybeParser
import jackrat.de.timklge.jackrat.Scanner
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class MaybeTests {
    @Test
    fun MaybeTest(){
        val input = "Hello"
        val scanner = Scanner(input)
        val helloParser = AtomParser("Hello", false)
        val helloMaybeParser = MaybeParser(helloParser)
        val node = helloMaybeParser.parse(scanner)
        assertEquals(helloMaybeParser, node.parser)
        assertEquals(1, node.children.count())
    }

    @Test
    fun MaybeIrregularTest(){
        val helloParser = AtomParser("Hello")
        val irregularInput = "Sonne"
        val irregularScanner = Scanner(irregularInput)
        val irregularParser = MaybeParser(helloParser)
        val irregularNode = irregularParser.parsePartial(irregularScanner)
        assertNotNull(irregularNode)
        assertEquals(irregularParser, irregularNode.parser)
        assertEquals(0, irregularNode.children.count())
    }
}