package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

data class ParserError(val parser: Parser, val line: Int, val column: Int, val position: Int, val failedParsers: List<Parser>?, val input: String) :
    Error() {

    override fun toString(): String {
        val startPos = if(position - 1 >= 0) position - 1 else 0
        val endPos = if(position + 10 < input.length) input.length - 1 else {
            if(input.length-1 >= 0) input.length - 1 else 0
        }

        return "Parser failed at line $line, column $column (position $position of input string) near ${input.slice(startPos..endPos)}"
    }
}

abstract class Parser(val nodeTransform: NodeTransform = ::emptyNodeTransform) {
    abstract val typeName: String

    abstract fun Match(s: Scanner): Node?

    fun parsePartial(originalScanner: Scanner): Node? {
        return originalScanner.applyRule(this)?.mapTransforms()
    }

    fun parse(originalScanner: Scanner): Node {
        val node = originalScanner.applyRule(this)?.mapTransforms()
        if(node != null){
            if(originalScanner.position < originalScanner.input.length){
                val consumed = originalScanner.input.substring(originalScanner.position)
                val line = consumed.count { it == '\n' } + 1
                val column = originalScanner.position - consumed.lastIndexOf('\n') + 1
                throw ParserError(this, line, column, originalScanner.position, null, originalScanner.input)
            }
            return node
        }

        var maxPos = 0
        val failedParsers: MutableList<Parser> = mutableListOf()
        for(index in originalScanner.input.length downTo 0){
            if(!originalScanner.memoization.containsKey(index) || originalScanner.memoization[index]!!.isEmpty()) continue
            maxPos = index
            failedParsers += originalScanner.memoization[index]!!.keys
            if(failedParsers.isNotEmpty()) break
        }
        if(maxPos >= originalScanner.input.length) maxPos = originalScanner.input.length - 1

        val consumed = originalScanner.input.substring(0..maxPos)
        val line = consumed.count { it == '\n'} + 1
        val lastBreak = if(consumed.lastIndexOf('\n') >= 0) consumed.lastIndexOf('\n') else 0
        val column = maxPos - lastBreak + 1

        throw ParserError(this, line, column, maxPos, failedParsers, originalScanner.input)
    }
}

abstract class ParserWithChildren(var children: List<Parser>, nodeFunc: NodeTransform = ::emptyNodeTransform) : Parser(nodeFunc){

}

