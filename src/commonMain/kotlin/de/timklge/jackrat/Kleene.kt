package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class KleeneParser(var subParser: Parser, var sepParser: Parser? = null, nodeTransform: NodeTransform = ::emptyNodeTransform) : ParserWithChildren(emptyList(), nodeTransform) {
    override val typeName: String = "KleeneParser"

    init {
        children = if(sepParser != null) {
            listOf(subParser, sepParser!!)
        } else {
            listOf(subParser)
        }
    }

    override fun Match(s: Scanner): Node? {
        val nodes = mutableListOf<Node>()
        var i = 0
        var lastValidPosition = s.position

        while(true){
            var matchedsep = false
            var sepnode: Node? = null
            if(i > 0 && sepParser != null){
                sepnode = s.applyRule(sepParser!!) ?: break
                matchedsep = true
            }
            i++

            val node = s.applyRule(subParser) ?: break
            if(matchedsep) nodes.add(sepnode!!)
            nodes.add(node)
            lastValidPosition = s.position
        }
        s.position = lastValidPosition

        val builder = StringBuilder()
        nodes.forEach { builder.append(it.matched) }
        val matched = builder.toString()

        return Node(matched, this, nodes)
    }
}