package jackrat.de.timklge.jackrat

import de.timklge.jackrat.Node
import de.timklge.jackrat.Parser

@OptIn(ExperimentalStdlibApi::class)
private fun getBreaks(input: String) = buildSet {
    val nonReadable = Regex("""[^\p{L}\p{N}\p{Pc}]""").findAll(input).map { it.range.first }.toSet()
    var previousWord = false
    input.forEachIndexed { index, _ ->
        val currentWord = !nonReadable.contains(index)
        if(!currentWord || !previousWord) add(index)
        previousWord = currentWord
    }
    add(input.count())
}

data class Scanner(val input: String, var position: Int, private val skipWhitespace: Boolean,
                   val memoization: MutableMap<Int, MutableMap<Parser, MemoEntry>>,
                   val heads: MutableMap<Int, Head>,
                   var invocationStack: Lr?,
                   val skipRegex: Regex?,
                   val breaks: Set<Int>) {

    constructor(input: String, position: Int = 0, skipWhitespace: Boolean = true) : this(input, position, skipWhitespace,
        mutableMapOf(), mutableMapOf(), null, if(skipWhitespace) Regex("""^[\r\n\t ]+""") else null, getBreaks(input)
    )

    constructor(input: String, position: Int = 0, skip: Regex) : this(input, position, true, mutableMapOf(), mutableMapOf(), null, skip, getBreaks(input))

    fun match(regex: Regex): String? {
        val matched = regex.find(input.substring(position))
        return if(matched != null){
            position += matched.value.length
            return matched.value
        } else null
    }

    fun skipWhitespace() {
        if(skipRegex != null) match(skipRegex)
    }

    fun isAtBreak(): Boolean {
        return breaks.contains(position)
    }

    fun recall(rule: Parser, pos: Int): MemoEntry? {
        val mmap: MutableMap<Parser, MemoEntry>? = memoization.getOrElse(pos, { null })
        val m: MemoEntry? = if(mmap != null) mmap[rule] else null
        val head = heads.getOrElse(pos, { return m })

        // Do not evaluate any rule that is not involved in this left recursion
        if(m == null && !head.involvedSet.contains(rule)) return MemoEntry(null, null, position)

        if(head.evalSet.contains(rule)){
            head.evalSet.remove(rule)
            val result = rule.Match(this)
            return MemoEntry(null, result, position)
        }

        return m
    }

    fun setupLr(rule: Parser, l: Lr) {
        if(l.head == null) l.head = Head(rule)
        var stack = invocationStack
        while(stack != null && stack.head != l.head){
            stack.head = l.head
            val newInvolved = mutableMapOf<Parser, Boolean>()
            l.head!!.involvedSet.forEach { newInvolved[it.key] = true }
            newInvolved[stack.rule] = true
            l.head!!.involvedSet = newInvolved
            stack = stack.next
        }
    }

    fun growLr(rule: Parser, p: Int, m: MemoEntry, h: Head): Node? {
        heads[p] = h
        while(true){
            position = p
            h.evalSet = mutableMapOf()
            h.involvedSet.forEach { h.evalSet[it.key] = it.value }
            val result = rule.Match(this)
            if(result == null || position <= m.Position) break
            m.lr = null
            m.Ans = result
            m.Position = position
        }
        heads.remove(p)
        position = m.Position
        return m.Ans
    }

    fun lrAnswer(rule: Parser, pos: Int, m: MemoEntry): Node? {
        val h = m.lr!!.head!!
        if(h.rule != rule) return m.lr!!.seed
        m.Ans = m.lr!!.seed
        m.lr = null
        if(m.Ans == null) return null
        return growLr(rule, pos, m, h)
    }

    fun applyRule(rule: Parser): Node? {
        val startPosition = position
        val mmap: MutableMap<Parser, MemoEntry> = if(memoization.contains(startPosition)) memoization[startPosition]!! else {
            val newmap = mutableMapOf<Parser, MemoEntry>()
            memoization[startPosition] = newmap
            newmap
        }
        val m = recall(rule, startPosition)
        if(m == null){
            val lr = Lr(null, rule, null, invocationStack)
            invocationStack = lr
            val memo = MemoEntry(lr, null, startPosition)
            mmap[rule] = memo
            val ans = rule.Match(this)
            invocationStack = invocationStack!!.next
            memo.Position = position
            if(lr.head != null){
                lr.seed = ans
                return lrAnswer(rule, startPosition, memo)
            }
            memo.lr = null
            memo.Ans = ans
            return ans
        }
        position = m.Position
        if(m.lr != null){
            setupLr(rule, m.lr!!)
            return m.lr!!.seed
        }
        return m.Ans
    }
}

data class Head(val rule: Parser, var involvedSet: MutableMap<Parser, Boolean>, var evalSet: MutableMap<Parser, Boolean>){
    constructor(rule: Parser) : this(rule, mutableMapOf(), mutableMapOf())
}

data class Lr(var seed: Node?, val rule: Parser, var head: Head?, val next: Lr?)

data class MemoEntry(var lr: Lr?, var Ans: Node?, var Position: Int)