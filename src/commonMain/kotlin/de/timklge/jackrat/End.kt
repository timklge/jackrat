package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class EndParser(val skipWhitespace: Boolean = true) : Parser() {
    override val typeName: String = "EndParser"

    override fun Match(s: Scanner): Node? {
        val startPosition = s.position
        if(skipWhitespace){
            s.skipWhitespace()
        }

        if(s.position == s.input.length){
            return Node("", this, listOf())
        }

        s.position = startPosition
        return null
    }
}