package de.timklge.jackrat

typealias NodeTransform = (node: Node) -> Node

fun emptyNodeTransform(node: Node) = node

open class Node(val matched: String, val parser: Parser, val children: List<Node>) {
    var parent: Node? = null

    fun findParser(p: Parser, skipParser: Parser? = null): Node? {
        return if(parser == p) this else findChildParser(p, skipParser)
    }

    fun findChildParser(p: Parser, skipParser: Parser? = null): Node? {
        children.forEach { child ->
            if(child.parser != skipParser) {
                val sub = child.findParser(p)
                if (sub != null) return sub
            }
        }
        return null
    }

    fun findParsers(vararg ps: Parser): List<Node> {
        return if(ps.contains(parser)) listOf(this) else findChildParsers(*ps)
    }

    fun findChildParsers(vararg ps: Parser): List<Node> {
        val result = mutableListOf<Node>()
        children.forEach { child ->
            val subs = child.findParsers(*ps)
            result.addAll(subs)
        }
        return result
    }

    fun containsParser(p: Parser): Boolean = findParser(p) != null

    fun findParserType(typeName: String): Node? {
        return if(parser.typeName == typeName) this else findChildParserType(typeName)
    }

    fun findChildParserType(typeName: String): Node? {
        children.forEach { child ->
            val sub = child.findParserType(typeName)
            if(sub != null) return sub
        }
        return null
    }

    internal fun mapTransforms(): Node {
        val node = Node(matched, parser, children.map { n ->
            n.parent = this
            val node = n.parser.nodeTransform(n)
            node.parent = this
            node
        })
        return parser.nodeTransform(node)
    }
}