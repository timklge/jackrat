package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

open class RegexParser(rs: String, caseInsensitive: Boolean = false, val skipWhitespace: Boolean = true, nodeTransform: NodeTransform = ::emptyNodeTransform) : Parser(nodeTransform) {
    override val typeName: String = "RegexParser"

    val regex = if(caseInsensitive) Regex("^$rs", RegexOption.IGNORE_CASE) else Regex("^$rs")

    override fun Match(s: Scanner): Node? {
        val startPosition = s.position
        if (skipWhitespace) {
            s.skipWhitespace()
            if (!s.isAtBreak()) {
                s.position = startPosition
                return null
            }
        }

        val matched = s.match(regex)
        if (matched == null) {
            s.position = startPosition
            return null
        }

        if (skipWhitespace) {
            if (!s.isAtBreak()) {
                s.position = startPosition
                return null
            }
        }

        return Node(matched, this, listOf())
    }
}