package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class MaybeParser(var subParser: Parser, nodeTransform: NodeTransform = ::emptyNodeTransform) : ParserWithChildren(listOf(subParser), nodeTransform) {
    override val typeName: String = "MaybeParser"

    override fun Match(s: Scanner): Node? {
        val startPosition = s.position
        val node = s.applyRule(subParser)
        if(node == null){
            s.position = startPosition
            return Node("", this, listOf())
        }
        return Node(node.matched, this, listOf(node))
    }
}