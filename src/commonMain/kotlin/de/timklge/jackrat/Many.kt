package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class ManyParser(var subParser: Parser?, var sepParser: Parser? = null, nodeTransform: NodeTransform = ::emptyNodeTransform) : ParserWithChildren(emptyList(), nodeTransform) {
    override val typeName: String = "ManyParser"

    init {
        children = if(subParser != null && sepParser != null) {
            listOf(subParser!!, sepParser!!)
        } else if(subParser != null){
            listOf(subParser!!)
        } else {
            emptyList()
        }
    }

    override fun Match(s: Scanner): Node? {
        var i = 0
        var lastValidPos = s.position
        var nodes = mutableListOf<Node>()

        while(true){
            var matchedsep = false
            var sepnode: Node? = null

            if(i > 0 && sepParser != null){
                sepnode = s.applyRule(sepParser!!)
                if(sepnode == null) break
                matchedsep = true
            }
            i++

            val node = s.applyRule(subParser!!) ?: break
            if(matchedsep) nodes.add(sepnode!!)

            nodes.add(node)
            lastValidPos = s.position
        }
        s.position = lastValidPos

        if(nodes.count() >= 1){
            val builder = StringBuilder()
            nodes.forEach { builder.append(it.matched) }
            val matched = builder.toString()
            return Node(matched, this, nodes)
        }

        return null
    }
}