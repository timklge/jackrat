package de.timklge.jackrat

class AtomParser(atom: String, caseInsensitive: Boolean = false, skipWs: Boolean = true, nodeFunc: NodeTransform = ::emptyNodeTransform)
    : RegexParser(Regex.escape(atom), caseInsensitive, skipWs, nodeFunc){
    override val typeName: String = "AtomParser"
}