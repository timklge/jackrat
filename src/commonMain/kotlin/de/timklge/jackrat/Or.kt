package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class OrParser(children: List<Parser>, nodeFunc: NodeTransform = ::emptyNodeTransform): ParserWithChildren(children, nodeFunc) {
    override val typeName: String = "OrParser"

    override fun Match(s: Scanner): Node? {
        val startPosition = s.position
        children.forEach {
            val node = s.applyRule(it)
            if(node != null){
                return Node(node.matched, this, listOf(node))
            }
            s.position = startPosition
        }
        return null
    }
}