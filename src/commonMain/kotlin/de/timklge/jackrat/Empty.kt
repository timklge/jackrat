package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class EmptyParser(transform: NodeTransform = ::emptyNodeTransform) : Parser(transform) {
    override val typeName: String = "EmptyParser"

    override fun Match(s: Scanner): Node? {
        return Node("", this, listOf())
    }
}

