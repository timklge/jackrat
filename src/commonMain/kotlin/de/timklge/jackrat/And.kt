package de.timklge.jackrat

import jackrat.de.timklge.jackrat.Scanner

class AndParser(children: List<Parser>, nodeFunc: NodeTransform = ::emptyNodeTransform) : ParserWithChildren(children, nodeFunc) {
    override val typeName: String = "AndParser"

    override fun Match(s: Scanner): Node? {
        val startPosition = s.position
        val nodes = children.map<Parser, Node> {
            val node = s.applyRule(it)
            if(node == null){
                s.position = startPosition
                return null
            }
            node
        }

        val builder = StringBuilder()
        nodes.forEach { builder.append(it.matched) }
        val matched = builder.toString()

        return Node(matched, this, nodes)
    }
}

